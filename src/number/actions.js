import { SET_VALUE, VALUE_OUT_OF_BOUNDS } from './constants';

export const setValue = value => ({
  type: SET_VALUE,
  payload: {
    value
  }
});

export const valueOutOfBounds = value => ({
  type: VALUE_OUT_OF_BOUNDS,
  payload: {
    value
  }
});
