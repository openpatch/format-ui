import React from 'react';
import { Trans } from '@lingui/macro';
import { Field } from '@openpatch/ui-core/lib/redux-form';
import NumberField from '@openpatch/ui-core/lib/NumberField';

class Form extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Field
          name="min"
          type="number"
          fullWidth
          helperText={<Trans>Leave empty to disable</Trans>}
          label={<Trans>Minimum Number</Trans>}
          component={NumberField}
        />
        <Field
          name="max"
          type="number"
          fullWidth
          label={<Trans>Maximum Number</Trans>}
          helperText={<Trans>Leave empty to disable</Trans>}
          component={NumberField}
        />
      </React.Fragment>
    );
  }
}

export default Form;
