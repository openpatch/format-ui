import React from 'react';
import PropTypes from 'prop-types';
import NumberField from '@openpatch/ui-core/lib/NumberField';

import { setValue, valueOutOfBounds } from './actions';

class Renderer extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    state: PropTypes.shape({
      value: PropTypes.number
    }),
    max: PropTypes.number,
    min: PropTypes.number
  };

  static defaultProps = {
    state: {
      value: ''
    },
    min: Number.MIN_SAFE_INTEGER,
    max: Number.MAX_SAFE_INTEGER
  };

  handleChange = value => {
    const { dispatch, min, max } = this.props;
    if (value == '' || (value >= Number(min) && value <= Number(max))) {
      dispatch(setValue(value));
    } else {
      dispatch(valueOutOfBounds(value));
    }
  };

  render() {
    const {
      state: { value },
      min,
      max
    } = this.props;
    return (
      <NumberField
        input={{
          value: value || 0,
          onChange: this.handleChange
        }}
        fullWidth
        meta={{}}
        max={Number(max)}
        min={Number(min)}
      />
    );
  }
}

export default Renderer;
