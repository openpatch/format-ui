export const SET_VALUE = 'openpatch.format.number.SET_VALUE';

export const VALUE_OUT_OF_BOUNDS =
  'openpatch.format.number.VALUE_OUT_OF_BOUNDS';
