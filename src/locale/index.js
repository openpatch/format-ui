import { mergeLocale } from '@openpatch/ui-core/lib/locale';
import uiCoreLocale from '@openpatch/ui-core/lib/locale';

const de = require('./de/messages.js');
const en = require('./en/messages.js');

export const locale = {
  de: mergeLocale(de, uiCoreLocale.de),
  en: mergeLocale(en, uiCoreLocale.en)
};

export default locale;
