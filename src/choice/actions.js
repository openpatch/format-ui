import { SET_VALUE } from './constants';

export const setValue = value => ({
  type: SET_VALUE,
  payload: {
    value
  }
});
