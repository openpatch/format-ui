import React from 'react';
import { Trans } from '@lingui/macro';
import { Field, FieldArray } from '@openpatch/ui-core/lib/redux-form';
import TextFieldArray from '@openpatch/ui-core/lib/TextFieldArray';
import Checkbox from '@openpatch/ui-core/lib/Checkbox';

class Form extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Field
          name="allowMultiple"
          label={<Trans> Allow multiple choices</Trans>}
          component={Checkbox}
        />
        <FieldArray fullWidth name="choices" component={TextFieldArray} />
      </React.Fragment>
    );
  }
}

export default Form;
