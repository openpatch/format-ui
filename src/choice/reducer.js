import produce from 'immer';
import { SET_VALUE } from './constants';

const initialState = {
  value: []
};

export default (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SET_VALUE:
        draft.value = action.payload.value;
        break;
    }
  });
