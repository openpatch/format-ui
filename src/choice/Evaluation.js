import React from 'react';
import PropTypes from 'prop-types';
import { Field } from '@openpatch/ui-core/lib/redux-form';
import ChoiceField from '@openpatch/ui-core/lib/ChoiceField';
import _get from 'lodash/get';

class Evaluation extends React.Component {
  static propTypes = {
    task: PropTypes.object
  };

  static defaultProps = {
    task: {
      data: {
        choices: []
      }
    }
  };

  render() {
    const { task } = this.props;
    const choices = _get(task, 'data.choices', []);
    const allowMultiple = _get(task, 'data.allowMultiple');
    return (
      <Field
        name="choices"
        options={choices}
        component={ChoiceField}
        allowMultiple={allowMultiple}
      />
    );
  }
}

export default Evaluation;
