import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Choice from '@openpatch/ui-core/lib/ResponseField/Choice';

import { setValue } from './actions';

class Renderer extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    state: PropTypes.shape({
      value: PropTypes.object
    }),
    choices: PropTypes.array,
    allowMultiple: PropTypes.bool
  };

  static defaultProps = {
    choices: [],
    allowMultiple: true,
    state: {
      value: []
    }
  };

  handleChange = value => {
    const { dispatch } = this.props;
    dispatch(setValue(value));
  };

  render() {
    const {
      state: { value },
      allowMultiple,
      ...props
    } = this.props;
    return (
      <Choice
        {...props}
        multiple={allowMultiple}
        value={value}
        onChange={this.handleChange}
      />
    );
  }
}

export default Renderer;
