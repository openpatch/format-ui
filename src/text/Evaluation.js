import React from 'react';
import { Field } from '@openpatch/ui-core/lib/redux-form';
import RegexField from '@openpatch/ui-core/lib/RegexField';

class Evaluation extends React.Component {
  render() {
    return <Field name="regex" component={RegexField} showTester fullWidth />;
  }
}

export default Evaluation;
