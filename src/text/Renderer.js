import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@openpatch/ui-core/lib/TextField';

import { setValue } from './actions';

class Renderer extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    state: PropTypes.shape({
      value: PropTypes.string
    }),
    multiline: PropTypes.bool
  };

  static defaultProps = {
    state: {
      value: ''
    }
  };

  handleChange = e => {
    const { dispatch } = this.props;
    dispatch(setValue(e.target.value));
  };

  render() {
    const {
      state: { value },
      multiline
    } = this.props;
    return (
      <TextField
        multiline={multiline}
        meta={{}}
        input={{
          value: value || '',
          onChange: this.handleChange
        }}
        fullWidth
      />
    );
  }
}

export default Renderer;
