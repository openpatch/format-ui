import React from 'react';
import { Trans } from '@lingui/macro';
import { Field } from '@openpatch/ui-core/lib/redux-form';
import Checkbox from '@openpatch/ui-core/lib/Checkbox';

class Form extends React.Component {
  render() {
    return (
      <Field
        name="multiline"
        label={<Trans>Multiline</Trans>}
        component={Checkbox}
      />
    );
  }
}

export default Form;
