import React from 'react';
import { connect } from 'react-redux';
import humps from 'humps';
import {
  reduxForm,
  FormSection as RFormSection,
  formValueSelector
} from '@openpatch/ui-core/lib/redux-form';
import FormSection from '@openpatch/ui-core/lib/FormSection';
import FormSectionHeader from '@openpatch/ui-core/lib/FormSectionHeader';
import FormSectionContent from '@openpatch/ui-core/lib/FormSectionContent';
import Button from '@material-ui/core/Button';
import Form from './Form';
import Evaluation from './Evaluation';
import MockRenderer from './MockRenderer';

class Mock extends React.Component {
  state = {
    solution: {},
    evaluateResponse: {
      correct: true
    },
    validateResponse: {
      correct: true
    }
  };

  evaluate = () => {
    const { task } = this.props;
    const { solution } = this.state;
    fetch('http://localhost:5001/v1/evaluate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        humps.decamelizeKeys({
          task,
          solution
        })
      )
    })
      .then(res => res.json())
      .then(json => {
        const result = humps.camelizeKeys(json);
        this.setState({ evaluateResponse: result });
      });
  };

  validate = () => {
    const { task } = this.props;
    fetch('http://localhost:5001/v1/validate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(humps.decamelizeKeys(task))
    })
      .then(res => res.json())
      .then(json => {
        const result = humps.camelizeKeys(json);
        this.setState({ validateResponse: result });
      });
  };

  handlePreviewChange = state => {
    this.setState({ solution: state });
  };

  render() {
    const { evaluation, task } = this.props;
    const { evaluateResponse, validateResponse } = this.state;
    return (
      <React.Fragment>
        <FormSection>
          <FormSectionHeader>Form</FormSectionHeader>
          <FormSectionContent>
            <RFormSection name="task.data" component={Form} task={task} />
          </FormSectionContent>
        </FormSection>
        <FormSection>
          <FormSectionHeader>Evaluation</FormSectionHeader>
          <FormSectionContent>
            <RFormSection
              name="task.evaluation"
              component={Evaluation}
              task={task}
            />
            <Button
              variant="contained"
              style={{
                backgroundColor: validateResponse.correct ? 'green' : 'red'
              }}
              fullWidth
              onClick={this.validate}
            >
              Validate
            </Button>
          </FormSectionContent>
        </FormSection>
        <FormSection>
          <FormSectionHeader>Preview</FormSectionHeader>
          <FormSectionContent style={{ alignItems: 'normal' }}>
            <MockRenderer
              onChange={this.handlePreviewChange}
              {...task}
              task="Test me!"
              key={JSON.stringify(task ? task.data : {})}
            />
            <Button
              variant="contained"
              style={{
                backgroundColor: evaluateResponse.correct ? 'green' : 'red'
              }}
              fullWidth
              onClick={this.evaluate}
            >
              Evaluate
            </Button>
          </FormSectionContent>
        </FormSection>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  evaluation: formValueSelector('bla')(state, 'evaluation'),
  task: formValueSelector('bla')(state, 'task')
});

const ReduxMock = reduxForm({ form: 'bla' })(connect(mapStateToProps)(Mock));

ReduxMock.displayName = 'Mock';

export default ReduxMock;
