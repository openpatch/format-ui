import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    padding: theme.spacing(1),
    background: theme.palette.background.paper,
    borderRadius: theme.shape.borderRadius,
    fontFamily: 'monospace',
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: theme.palette.primary.main
  }
});

class Block extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    text: PropTypes.string,
    isActive: PropTypes.bool
  };

  render() {
    const { text, classes, isActive } = this.props;
    return (
      <div
        className={classes.root}
        style={{ background: isActive ? '#E8E8E8' : '#FFF' }}
      >
        {text}
      </div>
    );
  }
}

export default withStyles(styles)(Block);
