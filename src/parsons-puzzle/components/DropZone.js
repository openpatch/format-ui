import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    minHeight: 250,
    padding: theme.spacing(1)
  }
});

class DropZone extends React.Component {
  render() {
    const { classes, children, ...props } = this.props;
    return (
      <div className={classes.root} {...props}>
        {children}
      </div>
    );
  }
}

export default withStyles(styles)(DropZone);
