import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Droppable } from 'react-beautiful-dnd';

import BlockDraggable from './BlockDraggable';
import Wrapper from './Wrapper';
import DropZone from './DropZone';

const styles = theme => ({
  wrapper: {
    width: 250,
    userSelect: 'none',
    paddingBottom: theme.spacing(1)
  },
  draggingOver: {
    backgroundColor: 'lightblue'
  },
  draggingFrom: {
    backgroundColor: 'red'
  }
});

class SourceDroppable extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    droppableId: PropTypes.string,
    blocks: PropTypes.array
  };

  static defaultProps = {
    blocks: []
  };

  render() {
    const { classes, blocks, droppableId } = this.props;
    return (
      <Droppable droppableId={droppableId}>
        {(dropProvided, dropSnapshot) => (
          <Wrapper
            {...dropProvided.droppableProps}
            isDraggingOver={dropSnapshot.isDraggingOver}
            isDraggingFrom={Boolean(dropSnapshot.draggingFromThisWith)}
          >
            <div ref={dropProvided.innerRef}>
              <DropZone>
                {blocks.map((block, i) => (
                  <BlockDraggable
                    key={block.id}
                    {...block}
                    index={i}
                    draggableId={`${droppableId}.${block.id}`}
                  />
                ))}
                {dropProvided.placeholder}
              </DropZone>
            </div>
          </Wrapper>
        )}
      </Droppable>
    );
  }
}

export default withStyles(styles)(SourceDroppable);
