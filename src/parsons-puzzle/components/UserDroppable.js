import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import SourceDroppable from './SourceDroppable';

const styles = () => ({
  root: {
    position: 'relative'
  },
  indentation: {
    position: 'absolute'
  }
});

class UserDroppable extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    maxIndentation: PropTypes.number,
    blocks: PropTypes.array,
    droppableId: PropTypes.string.isRequired
  };

  static defaultProps = {
    maxIndentation: 0,
    blocks: []
  };

  render() {
    const { classes, droppableId, blocks, maxIndentation } = this.props;

    const indentationDroppables = [];
    const blocksWithLine = blocks.map((block, i) => ({
      ...block,
      line: i
    }));
    for (let indentation = 0; indentation <= maxIndentation; indentation++) {
      indentationDroppables.push(
        <div
          key={indentation}
          className={classes.indentation}
          style={{
            zIndex: indentation,
            left: 64 * indentation,
            right: 0,
            top: 0,
            bottom: 0
          }}
        >
          <SourceDroppable
            droppableId={`${droppableId}#${indentation}`}
            blocks={blocksWithLine.filter(
              block => block.indentation == indentation
            )}
          />
        </div>
      );
    }
    return <div className={classes.root}>{indentationDroppables}</div>;
  }
}

export default withStyles(styles)(UserDroppable);
