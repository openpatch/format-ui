import React from 'react';
import PropTypes from 'prop-types';
import { DragDropContext } from 'react-beautiful-dnd';

import SourceDroppable from './components/SourceDroppable';
import UserDroppable from './components/UserDroppable';
import { initBlocks } from './actions';

class Renderer extends React.Component {
  static propTypes = {
    state: PropTypes.shape({
      userBlocks: PropTypes.array,
      sourceBlocks: PropTypes.array
    }),
    dispatch: PropTypes.func,
    maxIndentation: PropTypes.number,
    blocks: PropTypes.arrayOf(
      PropTypes.shape({
        link: PropTypes.number,
        lock: PropTypes.bool,
        indentation: PropTypes.number,
        hide: PropTypes.bool
      })
    )
  };

  static defaultProps = {
    state: {
      userBlocks: [],
      sourceBlocks: []
    },
    dispatch: () => {}
  };

  componentDidMount() {
    const { dispatch, blocks } = this.props;
    dispatch(initBlocks(blocks));
  }

  handleDragStart = () => {
    if (window.navigator.vibrate) {
      window.navigator.vibrate(100);
    }
  };

  handleDragEnd = result => {
    if (!result.destination) {
      return;
    }

    if (result.destination.index === result.source.index) {
      return;
    }

    const destinationdroppableId = result.destination.droppableId;
    const sourceDroppableId = result.source.droppableId;
  };

  render() {
    const {
      state: { userBlocks, sourceBlocks }
    } = this.props;
    return (
      <DragDropContext
        onDragStart={this.handleDragStart}
        onDragEnd={this.handleDragEnd}
      >
        <SourceDroppable blocks={sourceBlocks} droppableId="source" />
        <UserDroppable
          maxIndentation={0}
          blocks={userBlocks}
          droppableId="user"
        />
      </DragDropContext>
    );
  }
}

export default Renderer;
