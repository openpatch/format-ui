import Evaluation from './Evaluation';
import Form from './Form';
import Renderer from './Renderer';
import reducer from './reducer';

export default {
  label: 'Parsons Puzzle',
  Evaluation,
  Form,
  Renderer
};
