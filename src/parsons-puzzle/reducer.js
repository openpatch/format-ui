import produce from 'immer';

import move from './move';
import {
  INIT_BLOCKS,
  MOVE_WITHIN_USER,
  MOVE_WITHIN_SOURCE,
  MOVE_FROM_SOURCE_TO_USER,
  MOVE_FROM_USER_TO_SOURCE
} from './constants';

const initialState = {
  sourceBlocks: [],
  userBlocks: []
};

export default (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case INIT_BLOCKS:
        draft.sourceBlocks = action.payload.blocks.filter(
          block => !block.lock && !block.hide
        );
        draft.userBlocks = action.payload.blocks.filter(
          block => block.lock && !block.hide
        );
        break;
      case MOVE_WITHIN_USER:
        draft.userBlocks = move(
          state.userBlocks,
          action.payload.sourceId,
          action.payload.targetId
        );
        draft.userBlocks[action.payload.targetId].indentation =
          action.payload.indentation;
        break;
      case MOVE_WITHIN_SOURCE:
        draft.sourceBlocks = move(
          state.sourceBlocks,
          action.payload.sourceId,
          action.payload.targetId
        );
        break;
      case MOVE_FROM_USER_TO_SOURCE:
        draft.userBlocks.splice(action.payload.sourceId, 1);
        draft.sourceBlocks = [
          ...state.sourceBlocks.slice(0, action.payload.targetId),
          state.userBlocks[action.payload.sourceId],
          ...state.sourceBlocks.slice(action.payload.targetId)
        ];
        break;
      case MOVE_FROM_SOURCE_TO_USER:
        draft.sourceBlocks.splice(action.payload.sourceId, 1);
        draft.userBlocks = [
          ...state.userBlocks.slice(0, action.payload.targetId),
          state.sourceBlocks[action.payload.sourceId],
          ...state.userBlocks.slice(action.payload.targetId)
        ];
        draft.userBlocks[action.payload.targetId] = {
          ...draft.userBlocks[action.payload.targetId],
          indentation: action.payload.indentation
        };
        break;
    }
  });
