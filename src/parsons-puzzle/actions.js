import {
  INIT_BLOCKS,
  MOVE_FROM_SOURCE_TO_USER,
  MOVE_FROM_USER_TO_SOURCE,
  MOVE_WITHIN_USER,
  MOVE_WITHIN_SOURCE
} from './constants';

export const initBlocks = blocks => ({
  type: INIT_BLOCKS,
  payload: {
    blocks
  }
});

export const moveFromSourceToUser = (sourceId, targetId, indentation) => ({
  type: MOVE_FROM_SOURCE_TO_USER,
  payload: {
    sourceId,
    targetId,
    indentation
  }
});

export const moveFromUserToSource = (sourceId, targetId) => ({
  type: MOVE_FROM_USER_TO_SOURCE,
  payload: {
    sourceId,
    targetId
  }
});

export const moveWithinUser = (sourceId, targetId, indentation) => ({
  type: MOVE_WITHIN_USER,
  payload: {
    sourceId,
    targetId,
    indentation
  }
});

export const moveWithinSource = (sourceId, targetId) => ({
  type: MOVE_WITHIN_SOURCE,
  payload: {
    sourceId,
    targetId
  }
});
