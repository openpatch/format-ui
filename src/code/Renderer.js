import React from 'react';
import PropTypes from 'prop-types';
import TabbedCodeEditor from '@openpatch/ui-core/lib/TabbedCodeEditor';

import { setValueForId } from './actions';

class Renderer extends React.Component {
  static propTypes = {
    state: PropTypes.object,
    dispatch: PropTypes.func,
    sources: PropTypes.array
  };

  static defaultProps = {
    state: {},
    dispatch: () => {},
    sources: []
  };

  handleChange = (id, value) => {
    const { dispatch } = this.props;
    dispatch(setValueForId(id, value));
  };

  render() {
    const { state, sources } = this.props;
    const editors = sources
      .filter(source => source && source.fileName)
      .map(source => {
        const label = source.fileName;
        const value = state[label] || source.source;

        return {
          label,
          value
        };
      });
    return <TabbedCodeEditor editors={editors} onChange={this.handleChange} />;
  }
}

export default Renderer;
