import { SET_VALUE_FOR_ID } from './constants';

export const setValueForId = (id, value) => {
  return {
    type: SET_VALUE_FOR_ID,
    payload: {
      id,
      value
    }
  };
};
