import produce from 'immer';
import { SET_VALUE_FOR_ID } from './constants';

const initialState = {};

export default (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SET_VALUE_FOR_ID:
        draft[action.payload.id] = action.payload.value;
        break;
    }
  });
