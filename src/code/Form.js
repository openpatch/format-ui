import React from 'react';
import { FieldArray } from '@openpatch/ui-core/lib/redux-form';
import CodeFieldArray from '@openpatch/ui-core/lib/CodeFieldArray';

class Form extends React.Component {
  render() {
    return (
      <FieldArray
        fullWidth
        name="sources"
        label="Source Files"
        component={CodeFieldArray}
      />
    );
  }
}

export default Form;
