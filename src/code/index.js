import Form from './Form';
import Evaluation from './Evaluation';
import Renderer from './Renderer';
import reducer from './reducer';

export default {
  label: 'Code',
  Form,
  Evaluation,
  Renderer,
  reducer
};
