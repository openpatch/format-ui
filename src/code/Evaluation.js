import React from 'react';
import { Field, FieldArray } from '@openpatch/ui-core/lib/redux-form';
import CodeFieldArray from '@openpatch/ui-core/lib/CodeFieldArray';
import TextField from '@openpatch/ui-core/lib/TextField';
import MenuItem from '@material-ui/core/MenuItem';

const runners = [
  {
    image: 'registry.gitlab.com/openpatch/runner-java:latest',
    label: 'Java'
  }
];

class Evaluation extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Field
          name="image"
          label="Runner"
          fullWidth
          select
          component={TextField}
        >
          {runners.map(runner => (
            <MenuItem key={runner.image} value={runner.image}>
              {runner.label}
            </MenuItem>
          ))}
        </Field>
        <FieldArray
          fullWidth
          name="tests"
          label="Tests Files"
          component={CodeFieldArray}
        />
      </React.Fragment>
    );
  }
}

export default Evaluation;
