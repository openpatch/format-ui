import display from './display';
import fillIn from './fill-in';
import code from './code';
import parsonsPuzzle from './parsons-puzzle';
import ranking from './ranking';
import text from './text';
import number from './number';
import choice from './choice';

export default {
  display,
  'fill-in': fillIn,
  code,
  'parsons-puzzle': parsonsPuzzle,
  ranking,
  text,
  number,
  choice
};
