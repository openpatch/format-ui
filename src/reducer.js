import formats from './';

export default format => {
  return formats[format].reducer;
};
