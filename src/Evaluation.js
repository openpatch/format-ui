import React from 'react';
import PropTypes from 'prop-types';

import formats from './';

class FormatEvaluation extends React.Component {
  static propTypes = {
    task: PropTypes.shape({
      formatType: PropTypes.string.isRequired,
      evaluation: PropTypes.object,
      data: PropTypes.object
    })
  };

  static defaultProps = {
    task: {}
  };

  render() {
    const { task } = this.props;

    if (!task.formatType) {
      return null;
    }

    const Evaluation = formats[task.formatType].Evaluation;

    return <Evaluation {...this.props} />;
  }
}

export default FormatEvaluation;
