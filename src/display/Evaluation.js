import React from 'react';
import { Trans } from '@lingui/macro';

const Evaluation = () => <Trans>No evaluation possible</Trans>;
export default Evaluation;
