import React from 'react';
import PropTypes from 'prop-types';
import MarkdownRenderer from '@openpatch/ui-core/lib/MarkdownRenderer';

class Renderer extends React.Component {
  static propTypes = {
    source: PropTypes.string
  };
  render() {
    const { source } = this.props;
    return <MarkdownRenderer source={source} escapeHtml={false} />;
  }
}

export default Renderer;
