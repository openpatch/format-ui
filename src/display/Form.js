import React from 'react';
import { Trans } from '@lingui/macro';
import { Field } from '@openpatch/ui-core/lib/redux-form';
import CodeField from '@openpatch/ui-core/lib/CodeField';

class Form extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Field
          label={<Trans>Text</Trans>}
          name="source"
          mode="markdown"
          fullWidth
          helperText={<Trans>Markdown is supported</Trans>}
          component={CodeField}
        />
      </React.Fragment>
    );
  }
}

export default Form;
