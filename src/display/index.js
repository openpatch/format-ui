import Form from './Form';
import Renderer from './Renderer';
import Evaluation from './Evaluation';

export default {
  label: 'Display',
  Form,
  Renderer,
  Evaluation
};
