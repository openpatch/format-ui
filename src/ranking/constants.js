export const INIT_BLOCKS = 'openpatch.format.ranking.INIT_BLOCKS';

export const MOVE_FROM_SOURCE_TO_USER =
  'openpatch.format.ranking.MOVE_FROM_SOURCE_TO_USER';

export const MOVE_FROM_USER_TO_SOURCE =
  'openpatch.format.ranking.MOVE_FROM_USER_TO_SOURCE';

export const MOVE_WITHIN_USER = 'openpatch.format.ranking.MOVE_WITHIN_USER';

export const MOVE_WITHIN_SOURCE = 'openpatch.format.ranking.MOVE_WITHIN_SOURCE';
