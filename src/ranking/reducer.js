import produce from 'immer';

import move from './move';
import {
  INIT_BLOCKS,
  MOVE_WITHIN_USER,
  MOVE_WITHIN_SOURCE,
  MOVE_FROM_SOURCE_TO_USER,
  MOVE_FROM_USER_TO_SOURCE
} from './constants';

const initialState = {
  sourceBlocks: [],
  userBlocks: []
};

export default (state = initialState, action) =>
  produce(state, draft => {
    let sourceBlocks;
    let userBlocks;
    switch (action.type) {
      case INIT_BLOCKS:
        draft.sourceBlocks = action.payload.blocks
          .filter(block => block != undefined)
          .map((block, i) => ({
            id: i,
            text: block
          }));
        break;
      case MOVE_WITHIN_USER:
        draft.userBlocks = move(
          state.userBlocks,
          action.payload.sourceId,
          action.payload.targetId
        );
        break;
      case MOVE_WITHIN_SOURCE:
        draft.sourceBlocks = move(
          state.sourceBlocks,
          action.payload.sourceId,
          action.payload.targetId
        );
        break;
      case MOVE_FROM_USER_TO_SOURCE:
        draft.userBlocks.splice(action.payload.sourceId, 1);
        sourceBlocks = state.sourceBlocks || [];
        draft.sourceBlocks = [
          ...sourceBlocks.slice(0, action.payload.targetId),
          state.userBlocks[action.payload.sourceId],
          ...sourceBlocks.slice(action.payload.targetId)
        ];
        break;
      case MOVE_FROM_SOURCE_TO_USER:
        draft.sourceBlocks.splice(action.payload.sourceId, 1);
        userBlocks = state.userBlocks || [];
        draft.userBlocks = [
          ...userBlocks.slice(0, action.payload.targetId),
          state.sourceBlocks[action.payload.sourceId],
          ...userBlocks.slice(action.payload.targetId)
        ];
        break;
    }
  });
