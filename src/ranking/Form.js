import React from 'react';
import { FieldArray } from '@openpatch/ui-core/lib/redux-form';
import TextFieldArray from '@openpatch/ui-core/lib/TextFieldArray';

class Form extends React.Component {
  render() {
    return <FieldArray fullWidth name="blocks" component={TextFieldArray} />;
  }
}

export default Form;
