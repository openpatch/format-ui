import React from 'react';
import PropTypes from 'prop-types';
import { Trans } from '@lingui/macro';
import { withStyles } from '@material-ui/core/styles';
import { DragDropContext } from 'react-beautiful-dnd';
import Typography from '@material-ui/core/Typography';

import Droppable from './components/Droppable';
import {
  initBlocks,
  moveFromSourceToUser,
  moveFromUserToSource,
  moveWithinUser,
  moveWithinSource
} from './actions';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row'
  },
  container: {
    flex: 1,
    margin: theme.spacing(1)
  }
});

class Renderer extends React.Component {
  static propTypes = {
    state: PropTypes.shape({
      userBlocks: PropTypes.array,
      sourceBlocks: PropTypes.array
    }),
    dispatch: PropTypes.func,
    blocks: PropTypes.arrayOf(PropTypes.string)
  };

  static defaultProps = {
    state: {
      userBlocks: [],
      sourceBlocks: []
    },
    dispatch: () => {}
  };

  componentDidMount() {
    const { dispatch, blocks } = this.props;
    dispatch(initBlocks(blocks));
  }

  handleDragStart = () => {
    if (window.navigator.vibrate) {
      window.navigator.vibrate(100);
    }
  };

  handleDragEnd = result => {
    const { dispatch } = this.props;
    if (!result.destination) {
      return;
    }

    const di = result.destination.index;
    const si = result.source.index;
    const ddid = result.destination.droppableId;
    const sdid = result.source.droppableId;

    if (ddid == 'source' && sdid == 'user') {
      dispatch(moveFromUserToSource(si, di));
    } else if (ddid == 'source' && sdid == 'source') {
      dispatch(moveWithinSource(si, di));
    } else if (ddid == 'user' && sdid == 'source') {
      dispatch(moveFromSourceToUser(si, di));
    } else if (ddid == 'user' && sdid == 'user') {
      dispatch(moveWithinUser(si, di));
    }
  };

  render() {
    const {
      classes,
      state: { userBlocks, sourceBlocks }
    } = this.props;
    return (
      <DragDropContext
        onDragStart={this.handleDragStart}
        onDragEnd={this.handleDragEnd}
      >
        <div className={classes.root}>
          <div className={classes.container}>
            <Typography>
              <Trans>Pool</Trans>
            </Typography>
            <Droppable blocks={sourceBlocks} droppableId="source" />
          </div>
          <div className={classes.container}>
            <Typography>
              <Trans>Your choices</Trans>
            </Typography>
            <Droppable blocks={userBlocks} droppableId="user" />
          </div>
        </div>
      </DragDropContext>
    );
  }
}

export default withStyles(styles)(Renderer);
