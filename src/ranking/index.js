import Evaluation from './Evaluation';
import Form from './Form';
import Renderer from './Renderer';
import reducer from './reducer';

export default {
  label: 'Ranking',
  Evaluation,
  Form,
  Renderer,
  reducer
};
