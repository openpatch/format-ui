import React from 'react';
import PropTypes from 'prop-types';
import { Field } from '@openpatch/ui-core/lib/redux-form';
import _get from 'lodash/get';

import RendererField from '../RendererField';

class Evaluation extends React.Component {
  static propTypes = {
    task: PropTypes.object
  };

  static defaultProps = {
    task: {
      data: {
        blocks: null
      }
    }
  };

  render() {
    const { task } = this.props;
    const blocks = _get(task, 'data.blocks', []);
    return (
      <Field
        name="ranking"
        component={RendererField}
        key={JSON.stringify(task.data)}
        formatType="ranking"
        blocks={blocks}
      />
    );
  }
}

export default Evaluation;
