import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
    border: theme.spacing(1),
    paddingBottom: 0,
    userSelect: 'none',
    borderRadius: theme.shape.borderRadius,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.primary.dark,
    maxWidth: 500,
    flex: 1
  }
});

class Wrapper extends React.Component {
  render() {
    const {
      classes,
      children,
      isDraggingOver,
      isDraggingFrom,
      ...props
    } = this.props;
    let backgroundColor;
    if (isDraggingOver) {
      backgroundColor = 'rgba(0, 100, 0, 0.1)';
    }

    return (
      <div
        className={classes.root}
        style={{
          backgroundColor
        }}
        {...props}
      >
        {children}
      </div>
    );
  }
}

export default withStyles(styles)(Wrapper);
