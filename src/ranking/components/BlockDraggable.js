import React from 'react';
import PropTypes from 'prop-types';
import { Draggable } from 'react-beautiful-dnd';
import { withStyles } from '@material-ui/core/styles';

import Block from './Block';

const styles = theme => ({
  root: {
    marginBottom: theme.spacing(1)
  }
});

class BlockDraggable extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    text: PropTypes.string,
    draggableId: PropTypes.string.isRequired,
    index: PropTypes.number
  };

  render() {
    const { classes, draggableId, text, index } = this.props;
    return (
      <Draggable draggableId={draggableId} index={index}>
        {(dragProvided, dragSnapshot) => (
          <div
            ref={dragProvided.innerRef}
            className={classes.root}
            {...dragProvided.draggableProps}
            {...dragProvided.dragHandleProps}
          >
            <Block text={text} active={dragSnapshot.isDragging} />
          </div>
        )}
      </Draggable>
    );
  }
}

export default withStyles(styles)(BlockDraggable);
