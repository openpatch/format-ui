import React from 'react';
import PropTypes from 'prop-types';
import { Trans } from '@lingui/macro';
import { Field } from '@openpatch/ui-core/lib/redux-form';
import RegexField from '@openpatch/ui-core/lib/RegexField';

class Evaluation extends React.Component {
  static propTypes = {
    task: PropTypes.object
  };

  static defaultProps = {
    task: {}
  };
  render() {
    const {
      task: { data }
    } = this.props;

    let source = '';
    let styles = {};
    if (data) {
      source = data.source || '';
      styles = data.styles || {};
    }

    const el = document.createElement('html');
    el.innerHTML = source;
    const inputs = [...el.getElementsByTagName('input')];
    const fields = [];
    const elementsId = [];

    inputs.forEach((input, index) => {
      const syncId = input.getAttribute('sync');
      const elementId = syncId ? `s${syncId}` : `n${index}`;
      if (elementsId.indexOf(elementId) == -1) {
        elementsId.push(elementId);
        const label = syncId ? (
          <Trans>Regular Expression for synced inputs with id {syncId}</Trans>
        ) : (
          <Trans>Regular Expression for input {index}</Trans>
        );
        fields.push(
          <Field
            fullWidth
            key={elementId}
            name={elementId}
            label={label}
            showTester
            component={RegexField}
          />
        );
        fields.push(<div key={elementId + 'spacer'} style={{ height: 30 }} />);
      }
    });

    return <React.Fragment>{fields}</React.Fragment>;
  }
}

export default Evaluation;
