import Form from './Form';
import Renderer from './Renderer';
import reducer from './reducer';
import Evaluation from './Evaluation';

export default {
  label: 'Fill in',
  Form,
  Renderer,
  reducer,
  Evaluation
};
