import React from 'react';
import PropTypes from 'prop-types';
import CodeBlock from '@openpatch/ui-core/lib/CodeBlock';

class CodeBlockWithInputs extends React.PureComponent {
  static propTypes = {
    value: PropTypes.string
  };

  static defaultProps = {
    value: ''
  };
  render() {
    const { value, ...props } = this.props;
    const dangerouslySetInnerHTML = { __html: value };
    return (
      <CodeBlock {...props} dangerouslySetInnerHTML={dangerouslySetInnerHTML} />
    );
  }
}

export default CodeBlockWithInputs;
