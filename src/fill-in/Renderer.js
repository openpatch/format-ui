import React from 'react';
import PropTypes from 'prop-types';
import MarkdownRenderer from '@openpatch/ui-core/lib/MarkdownRenderer';

import CodeBlockWithInputs from './CodeBlockWithInputs';
import { setValueForId } from './actions';

class Renderer extends React.Component {
  static propTypes = {
    state: PropTypes.object,
    dispatch: PropTypes.func,
    source: PropTypes.string.isRequired,
    styles: PropTypes.arrayOf(
      PropTypes.shape({
        size: PropTypes.number,
        options: PropTypes.arrayOf(PropTypes.string)
      })
    )
  };

  static defaultProps = {
    state: {},
    dispatch: () => {},
    styles: []
  };

  setRef = el => {
    this.inputsEl = el;
  };

  onFillInChange = e => {
    const { dispatch } = this.props;
    const input = e.target;
    const id = input.id;
    const value = input.value;
    dispatch(setValueForId(id, value));
  };

  componentDidUpdate() {
    const { state } = this.props;

    const inputs = [
      ...this.inputsEl.getElementsByTagName('input'),
      ...this.inputsEl.getElementsByTagName('select')
    ];
    inputs.forEach(input => {
      input.value = state[input.id] || '';
    });
  }

  componentDidMount() {
    const { styles, state } = this.props;
    const inputs = [...this.inputsEl.getElementsByTagName('input')];

    // replace dummy input elements with real ones.
    inputs.forEach((input, index) => {
      let element = document.createElement('input');
      element.size = 10;
      const styleId = input.getAttribute('style');
      if (styleId) {
        const style = styles[styleId] || {};
        if (style.options && style.options.length > 0) {
          element = document.createElement('select');
          style.options.forEach(option => {
            const elOption = document.createElement('option');
            elOption.value = option;
            elOption.innerText = option;
            element.appendChild(elOption);
          });
        } else {
          element.size = style.size || 10;
        }
        element.setAttribute('style', styleId);
      }
      const syncId = input.getAttribute('sync');
      element.id = syncId ? `s${syncId}` : `n${index}`;
      if (state[element.id]) {
        element.value = state[element.id];
      }
      element.addEventListener('change', this.onFillInChange);
      element.addEventListener('keyup', this.onFillInChange);

      input.parentNode.replaceChild(element, input);
    });
  }

  componentWillUnmount() {
    const inputs = [
      ...this.inputsEl.getElementsByTagName('input'),
      ...this.inputsEl.getElementsByTagName('select')
    ];
    inputs.forEach(input => {
      input.removeEventListener('change', this.onFillInChange);
      input.removeEventListener('keyup', this.onFillInChange);
    });
  }

  render() {
    const { source } = this.props;
    return (
      <div ref={this.setRef}>
        <MarkdownRenderer
          source={source}
          escapeHtml={false}
          renderers={{ code: CodeBlockWithInputs }}
        />
      </div>
    );
  }
}

export default Renderer;
