import React from 'react';
import PropTypes from 'prop-types';
import { t } from '@lingui/macro';
import { i18n } from '@openpatch/ui-core/lib/I18nProvider';
import { Trans } from '@lingui/macro';
import { Field, FieldArray } from '@openpatch/ui-core/lib/redux-form';
import CodeField from '@openpatch/ui-core/lib/CodeField';
import TextField from '@openpatch/ui-core/lib/TextField';
import FormSection from '@openpatch/ui-core/lib/FormSection';
import FormSectionHeader from '@openpatch/ui-core/lib/FormSectionHeader';
import FormSectionContent from '@openpatch/ui-core/lib/FormSectionContent';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Tooltip from '@material-ui/core/Tooltip';

class StyleFields extends React.Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    styles: PropTypes.array
  };

  static defaultProps = {
    styles: []
  };

  moveStyleDown = index => {
    const { fields } = this.props;
    if (index < fields.length - 1) {
      fields.swap(index, index + 1);
    }
  };

  moveStyleUp = index => {
    const { fields } = this.props;
    if (index > 0) {
      fields.swap(index, index - 1);
    }
  };

  deleteStyle = index => {
    const { fields } = this.props;
    fields.remove(index);
  };

  addStyle = () => {
    const { fields } = this.props;
    fields.push();
  };

  render() {
    const { fields } = this.props;
    return (
      <React.Fragment>
        {fields.map((styleField, index) => (
          <FormSection key={index}>
            <FormSectionHeader>
              <Trans>Style {index}</Trans>
              <Tooltip title={<Trans>Move Style {index} Up</Trans>}>
                <IconButton onClick={() => this.moveStyleUp(index)}>
                  <Icon>arrow_upward</Icon>
                </IconButton>
              </Tooltip>
              <Tooltip title={<Trans>Move Style {index} Down</Trans>}>
                <IconButton onClick={() => this.moveStyleDown(index)}>
                  <Icon>arrow_downward</Icon>
                </IconButton>
              </Tooltip>
              <Tooltip title={<Trans>Delete Style {index}</Trans>}>
                <IconButton onClick={() => this.deleteStyle(index)}>
                  <Icon>delete</Icon>
                </IconButton>
              </Tooltip>
            </FormSectionHeader>
            <FormSectionContent>
              <Field
                label={<Trans>Size</Trans>}
                name={`${styleField}.size`}
                fullWidth
                type="number"
                component={TextField}
              />
              <FieldArray
                label={<Trans>Options</Trans>}
                name={`${styleField}.options`}
                component={FieldOptions}
              />
            </FormSectionContent>
          </FormSection>
        ))}
        <Button onClick={this.addStyle}>Add Style</Button>
      </React.Fragment>
    );
  }
}

class FieldOptions extends React.Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    meta: PropTypes.object
  };
  moveOptionDown = index => {
    const { fields } = this.props;
    if (index < fields.length - 1) {
      fields.swap(index, index + 1);
    }
  };

  moveOptionUp = index => {
    const { fields } = this.props;
    if (index > 0) {
      fields.swap(index, index - 1);
    }
  };

  deleteOption = index => {
    const { fields } = this.props;
    fields.remove(index);
  };

  addOption = () => {
    const { fields } = this.props;
    fields.push();
  };

  render() {
    const { fields } = this.props;
    return (
      <React.Fragment>
        {fields.map((choice, index) => (
          <div key={index} style={{ display: 'flex', width: '100%' }}>
            <Field
              name={`${choice}`}
              label={<Trans>Option</Trans>}
              fullWidth
              multiline
              component={TextField}
            />
            <Tooltip title={<Trans>Move Option {index} Up</Trans>}>
              <IconButton onClick={() => this.moveOptionUp(index)}>
                <Icon>arrow_upward</Icon>
              </IconButton>
            </Tooltip>
            <Tooltip title={<Trans>Move Option {index} Down</Trans>}>
              <IconButton onClick={() => this.moveOptionDown(index)}>
                <Icon>arrow_downward</Icon>
              </IconButton>
            </Tooltip>
            <Tooltip title={<Trans>Delete Option {index}</Trans>}>
              <IconButton onClick={() => this.deleteOption(index)}>
                <Icon>delete</Icon>
              </IconButton>
            </Tooltip>
          </div>
        ))}
        <Button onClick={this.addOption}>Add Option</Button>
      </React.Fragment>
    );
  }
}

class Form extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Field
          label={<Trans>Markdown</Trans>}
          name="source"
          fullWidth
          mode="markdown"
          helperText={i18n._(t`
                  Use <input stlye="style_id" sync="synchronize_id" /> to insert a fill-in
                  field. Markdown is supported.`)}
          component={CodeField}
        />
        <FieldArray name="styles" component={StyleFields} />
      </React.Fragment>
    );
  }
}

export default Form;
