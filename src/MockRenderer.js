import React from 'react';
import PropTypes from 'prop-types';
import Renderer from './Renderer';
import formats from './';
import reducer from './reducer';

class MockRenderer extends React.Component {
  state = {};

  static propTypes = {
    onChange: PropTypes.func
  };

  static defaultProps = {
    onChange: () => {}
  };

  dispatch = action => {
    const { onChange, formatType } = this.props;
    if (formatType in formats) {
      const newState = reducer(formatType)(this.state, action);
      this.setState(newState, () => {
        onChange(newState);
      });
    }
  };

  render() {
    return (
      <Renderer {...this.props} dispatch={this.dispatch} state={this.state} />
    );
  }
}

export default MockRenderer;
