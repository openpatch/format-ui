import React from 'react';
import PropTypes from 'prop-types';
import _isEqual from 'lodash/isEqual';
import formats from './';
import reducer from './reducer';

class RendererField extends React.Component {
  static propTypes = {
    input: PropTypes.shape({
      value: PropTypes.object.isRequired,
      onChange: PropTypes.func.isRequired
    }),
    formatType: PropTypes.string.isRequired
  };

  static defaultProps = {};

  dispatch = action => {
    const {
      input: { value, onChange },
      formatType
    } = this.props;
    if (formatType in formats) {
      const oldValue = value === '' ? undefined : value;
      const newValue = reducer(formatType)(oldValue, action);
      onChange(newValue);
    }
  };

  componentDidUpdate({ input, meta, formatType, ...prevProps }) {
    const {
      input: { onChange },
      meta: m,
      formatType: f,
      ...props
    } = this.props;
    if (!_isEqual(prevProps, props)) {
      onChange(undefined);
    }
  }

  render() {
    const {
      input: { value },
      formatType
    } = this.props;
    const state = value === '' ? undefined : value;

    if (formatType in formats) {
      const FormatRenderer = formats[formatType].Renderer;
      return (
        <div style={{ width: '100%' }}>
          <FormatRenderer
            {...this.props}
            dispatch={this.dispatch}
            state={state}
          />
        </div>
      );
    }
    return 'Not Supported';
  }
}

export default RendererField;
