import React from 'react';
import PropTypes from 'prop-types';

import formats from './';

class FormatForm extends React.Component {
  static propTypes = {
    task: PropTypes.shape({
      formatType: PropTypes.string.isRequired,
      evaluation: PropTypes.object,
      data: PropTypes.object
    })
  };

  static defaultProps = {
    task: {}
  };

  render() {
    const { task } = this.props;

    if (!task.formatType) {
      return null;
    }

    const Form = formats[task.formatType].Form;

    return <Form {...task} />;
  }
}

export default FormatForm;
