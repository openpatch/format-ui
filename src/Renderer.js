import React from 'react';
import PropTypes from 'prop-types';
import _isEmpty from 'lodash/isEmpty';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import MarkdownRenderer from '@openpatch/ui-core/lib/MarkdownRenderer';

import formats from './';

const styles = theme => ({
  header: {
    padding: theme.spacing(3),
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.primary.contrastText,
    display: 'flex',
    alignItems: 'center'
  },
  body: {
    padding: theme.spacing(3)
  }
});

class Renderer extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    formatType: PropTypes.string,
    task: PropTypes.string,
    data: PropTypes.object,
    dispatch: PropTypes.func,
    state: PropTypes.object
  };

  static defaultProps = {
    formatType: 'common',
    task: '',
    data: {},
    dispatch: () => {},
    state: {}
  };

  render() {
    const { classes, task, data, formatType, dispatch, state } = this.props;
    const format = formats[formatType];
    if (!format) {
      return null;
    }

    const FormatRenderer = formats[formatType].Renderer;
    return (
      <Paper>
        <div className={classes.header}>
          <Typography color="inherit" component="div">
            <MarkdownRenderer source={task} escapeHtml={false} />
          </Typography>
        </div>
        {!_isEmpty(data) && (
          <div className={classes.body}>
            <FormatRenderer {...data} dispatch={dispatch} state={state} />
          </div>
        )}
      </Paper>
    );
  }
}

export default withStyles(styles)(Renderer);
