import {
  initBlocks,
  moveWithinUser,
  moveWithinSource,
  moveFromSourceToUser,
  moveFromUserToSource
} from '../../src/parsons-puzzle/actions';
import reducer from '../../src/parsons-puzzle/reducer';

const mockState = {
  userBlocks: [
    {
      lock: true,
      text: 'u1',
      indentation: 0
    },
    {
      link: 2,
      text: 'u2',
      indentation: 1
    }
  ],
  sourceBlocks: [
    {
      link: 1,
      text: 's1'
    },
    {
      text: 's2'
    },
    {
      link: 1,
      text: 's3'
    },
    {
      link: 2,
      text: 's4'
    }
  ]
};

describe('parsons-puzzle reducer', () => {
  it('should initBlocks', () => {
    const userBlocks = [
      {
        lock: true,
        text: 'u'
      }
    ];

    const sourceBlocks = [
      {
        link: 1,
        text: 's'
      },
      {
        text: 's'
      }
    ];

    const hiddenBlocks = [
      {
        hide: true,
        text: 'h'
      }
    ];

    const blocks = [...userBlocks, ...sourceBlocks, ...hiddenBlocks];
    const newState = reducer(undefined, initBlocks(blocks));

    expect(newState.sourceBlocks).toEqual(sourceBlocks);
    expect(newState.userBlocks).toEqual(userBlocks);
  });

  it('should moveWithinUser', () => {
    const newState = reducer(mockState, moveWithinUser(1, 0, 1));
    expect(newState.userBlocks[0].text).toEqual('u2');
    expect(newState.userBlocks[0].indentation).toEqual(1);
    expect(newState.userBlocks.length).toEqual(mockState.userBlocks.length);
  });

  it('should moveWithinSource', () => {
    const newState = reducer(mockState, moveWithinSource(1, 0));
    expect(newState.sourceBlocks[0].text).toEqual('s2');
    expect(newState.sourceBlocks.length).toEqual(mockState.sourceBlocks.length);
  });

  it('should moveFromUserToSource', () => {
    const newState = reducer(mockState, moveFromUserToSource(1, 0));
    expect(newState.sourceBlocks[0].text).toEqual('u2');
    expect(newState.userBlocks.length).toEqual(mockState.userBlocks.length - 1);
    expect(newState.sourceBlocks.length).toEqual(
      mockState.sourceBlocks.length + 1
    );
  });

  it('should moveFromSourceToUser', () => {
    const newState = reducer(mockState, moveFromSourceToUser(1, 0, 4));
    expect(newState.userBlocks[0].text).toEqual('s2');
    expect(newState.userBlocks[0].indentation).toEqual(4);
    expect(newState.userBlocks.length).toEqual(mockState.userBlocks.length + 1);
    expect(newState.sourceBlocks.length).toEqual(
      mockState.sourceBlocks.length - 1
    );
  })
});
