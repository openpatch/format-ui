import {
  initBlocks,
  moveWithinUser,
  moveWithinSource,
  moveFromSourceToUser,
  moveFromUserToSource
} from '../../src/parsons-puzzle/actions';

describe('parsons puzzle actions', () => {
  it('should create init blocks action', () => {
    const blocks = [
      {
        link: 1
      },
      {
        lock: true
      },
      {
        hide: true
      }
    ];
    expect(initBlocks(blocks)).toMatchSnapshot();
  });

  it('should create moveWithinUser action', () => {
    expect(
      moveWithinUser({
        targetId: 0,
        sourceId: 1,
        indentation: 1
      })
    ).toMatchSnapshot();
  });

  it('should create moveWithinSource action', () => {
    expect(
      moveWithinSource({
        targetId: 0,
        sourceId: 1
      })
    ).toMatchSnapshot();
  });

  it('should create moveFromSourceToUser action', () => {
    expect(
      moveFromSourceToUser({
        targetId: 0,
        sourceId: 1,
        indentation: 1
      })
    ).toMatchSnapshot();
  });

  it('should create moveFromUserToSource action', () => {
    expect(
      moveFromUserToSource({
        targetId: 0,
        sourceId: 1
      })
    ).toMatchSnapshot();
  });
});
