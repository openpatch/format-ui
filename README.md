# frontend

[![Pipeline][pipeline-badge]][pipeline]
[![npm package][npm-badge]][npm]
[![Coverage][coverage-badge]][coverage]

# Setup

First we need to install all dependencies.
```
yarn install
```

Then we can start [react-cosmos](https://github.com/react-cosmos/react-cosmos).
```
yarn start
```

Afterwards we can start developing our components.

[pipeline-badge]: https://gitlab.com/openpatch/ui-core/badges/master/pipeline.svg
[pipeline]: https://gitlab.com/openpatch/ui-core/commits/master

[npm-badge]: https://badge.fury.io/js/%40openpatch%2Fui-core.svg
[npm]: https://www.npmjs.com/package/@openpatch/ui-core

[coverage-badge]: https://gitlab.com/openpatch/ui-core/badges/master/coverage.svg
[coverage]: https://gitlab.com/openpatch/ui-core/commits/master
