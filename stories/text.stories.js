import makeFormatStories from './makeFormatStories';

const task = {
  formatType: 'text',
  data: {
    multiline: false
  }
};

makeFormatStories('Text', task);

