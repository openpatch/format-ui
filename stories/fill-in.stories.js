import makeFormatStories from './makeFormatStories';

const task = {
  formatType: 'fill-in',
  data: {
    source:
      '# Test\n\n**hi** <input sync="1" /> <input /> <input sync="2" /> <input sync="1" />'
  }
};

makeFormatStories('Fill In', task);

