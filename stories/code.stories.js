import makeFormatStories from './makeFormatStories';

const task = {
  formatType: 'code',
  data: {
    sources: [{ fileName: 'House.java' }, { fileName: 'Car.java' }]
  }
};

makeFormatStories('Code', task);
