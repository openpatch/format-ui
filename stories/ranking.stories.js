import makeFormatStories from './makeFormatStories';

const task = {
  formatType: 'ranking',
  data: {
    blocks: ['Block 1', 'Block 2', 'Block 3']
  }
};

makeFormatStories('Ranking', task);
