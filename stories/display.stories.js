import makeFormatStories from './makeFormatStories';

const task = {
  formatType: 'display',
  data: {
    source: '# Test\n\n**hi**'
  }
};

makeFormatStories('Display', task);

