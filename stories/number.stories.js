import makeFormatStories from './makeFormatStories';

const task = {
  formatType: 'number',
  data: {
    min: 2,
    max: 10
  }
};

makeFormatStories('Number', task);
