import { boolean } from '@storybook/addon-knobs';

import makeFormatStories from './makeFormatStories';

const task = {
  formatType: 'choice',
  data: {
    choices: ['test', 'test 2'],
    allowMultiple: boolean('allowMultiple', false)
  }
};

makeFormatStories('Choice', task);
