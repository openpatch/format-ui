import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import withReduxFormProvider from '@openpatch/ui-core/lib/storybookProvider/withReduxFormProvider';
import Mock from '../src/Mock';
import MockRenderer from '../src/MockRenderer';

export default (name, task) => {
  const packageName = name.toLowerCase().replace(' ', '-');
  const Evaluation = require(`../src/${packageName}/Evaluation`).default;
  const Form = require(`../src/${packageName}/Form`).default;
  const Renderer = require(`../src/${packageName}/Renderer`).default;
  return storiesOf(name, module)
    .addDecorator(withReduxFormProvider())
    .add('Evaluation', () => <Evaluation task={task} />)
    .add('Form', () => <Form />)
    .add('Renderer', () => (
      <Renderer {...task.data} dispatch={action('dispatch')} />
    ))
    .add('MockRenderer', () => (
      <MockRenderer {...task} dispatch={action('dispatch')} />
    ))
    .add('Mock', () => <Mock initialValues={{ task }} />);
};
